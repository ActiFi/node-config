
# Why?

This is a simple config loader for node applications.
Drop a config.json file in project root.


## Install

In package.json, under dependencies, you can do...

```"config": "git@gitlab.com:ActiFi/node-config.git"```

## Usage

```
var config = require('config');
config.get('SOME:URL');
config.get('SOME:URL', 'http://www.google.com'); // will get the value or return a default if needed
```