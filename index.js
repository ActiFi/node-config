
const _ = require('lodash');

let configJson = require('./../../config.json');

exports.getConfig = function(){
  return configJson;
};
exports.setConfig = function(conf){
  configJson = conf;
};

exports.get = function(key, defaultValue){
  let val = _.get(configJson, key, defaultValue);
  if(_.isUndefined(val))
    throw new Error('Config [' + key + '] is invalid.');
  return val;
};